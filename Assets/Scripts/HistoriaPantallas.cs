using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HistoriaPantallas : MonoBehaviour
{
    public int siguiente;
    public float tiempo;
    public GameObject frame1;
    public GameObject video1;
    public GameObject video2;
    public GameObject video3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Frame();
    }

    public void Frame()
    {
       
        frame1.SetActive(false);
    }

    public void IniciaVideo()
    {
        siguiente += 1;
        switch (siguiente)
        {
            case 1:

                //video1.SetActive(false);
                video2.SetActive(true);
                break;
            case 2:

                //video2.SetActive(false);
                video3.SetActive(true);
                break;
            case 3:

                SceneManager.LoadScene("Nivel0"); 
                Cursor.lockState = CursorLockMode.Locked;
                Time.timeScale = 1f;
                break;

        }
    }
}
