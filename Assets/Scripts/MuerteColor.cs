using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuerteColor : MonoBehaviour
{

    public RawImage ri;
    public GameObject Panel;
    public float transparencia;
    // Start is called before the first frame update
    void Start()
    {
        ri = GetComponent<RawImage>();
    }

    // Update is called once per frame
    void Update()
    {
        Color nuevoColor = new Color(1, 1, 1, transparencia);

        Panel.GetComponent<RawImage>().color = nuevoColor;
    }
}
